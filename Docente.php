<?php
    require_once 'Conexion.php';
    class Docente{
        public $conexion;

       
        public function Registro($Name,$Ape,$Direccion){
            $conectarv = new conexionPDO();
            $conexion = $conectarv->Conectar();
            $SQL = "INSERT INTO docentes (Nombre,Apellido,Direccion) values (:nombre,:Apellido,:Direccion)";
            $statement = $conexion->prepare($SQL);
            $statement->bindParam(':nombre',$Name);
            $statement->bindParam(':Apellido',$Ape);
            $statement->bindParam(':Direccion',$Direccion);
            
            if(!$statement){
                return 'Error en el registro';
            }else {
                $statement->execute();
                return 'Registro exitoso';
                
            }


        }

        public function Consulta(){
            $arreglo=null;
            $conectarv = new conexionPDO();
            $conexion = $conectarv->Conectar();
            $SQL = "SELECT Nombre,Apellido,Direccion,idD FROM docentes";
            $statement = $conexion->prepare($SQL);
            $statement->execute();

            while($resultado = $statement->fetch()){    
                $arreglo[]=$resultado;
            }
            return  $arreglo;
        }

        public function Eliminar($DocenteID){
            $conectarv = new conexionPDO();
            $conexion = $conectarv->Conectar();
            $SQL = "delete from docentes where idD = :idDP";
            $stado = $conexion->prepare($SQL);
            $stado->bindParam(':idDP',$DocenteID);
            if(!$stado){
                return 'Error al eliminar docente';
            }else {
                $stado->execute();
                return 'Registro eliminado';
                
                
            }

        }

        public function BuscarDocente($nombreD){
            $arreglo=null;
            $conectarv = new conexionPDO();
            $conexion = $conectarv->Conectar();
            $nombre = "%".$nombreD."%";
            $SQL = "select * from docentes where Nombre like :nombre";
            $statement = $conexion->prepare($SQL);
            $statement->bindParam(":nombre",$nombre);
            $statement->execute();

            while($resultado = $statement->fetch()){    
                $arreglo[]=$resultado;
            }
            return  $arreglo;

        }

    }


?>