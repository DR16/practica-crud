<!DOCTYPE html>
    <html>
    <head>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
        <style>
            *{
                margin:0;
                padding:0;
                list-style:none;
            }
            li{
                float:left;
            }
            ul{
                overflow:hidden;
            }
            a{
                display:block;
                padding:10px;
                text-decoration: none;
                color:black;
                
            }
            header{
                background-color:#41817F;
            }
        </style>

    </head>
    <body>
        <header>
            <div id="Navegacion">
                <nav> 
                    <ul>
                        <li><a href='index.php'>Ingreso de docente</a></li>
                        <li><a href='VerDocentes.php'>Buscar docente</a></li>
                    </ul>
                </nav>
            </div>
        </header>
    </body>
    </html>